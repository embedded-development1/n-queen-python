from queen import Board

def start():
    repeat = True
    queenBoard = Board()
    while repeat:
        numOfQueens = 0
        while numOfQueens < 1 or numOfQueens > 10:
            try:
                numOfQueens = int(input("How many queens? [1-10]: "))
            except ValueError:
                print("Not a number")
        boardSize = 0
        while boardSize < 1 or boardSize > 10:
            try:
                boardSize = int(input("How big is the board? [1-10]: "))
            except ValueError:
                print("Not a number")
        firstPlacement = 0
        while firstPlacement < 1 or firstPlacement > boardSize:
            try:
                firstPlacement = int(input("Place the queen in what row? [1-" + str(boardSize) + "]: "))
            except ValueError:
                print("Not a number")
        firstPlacement = firstPlacement - 1
        queenBoard.PopulateBoard(boardSize, numOfQueens, firstPlacement)
        nextstep = input("Try again? (y or n)")
        if(nextstep == "no" or nextstep == "n" or nextstep == "N" or nextstep == "No"):
            repeat = False

if __name__ =="__main__":
    start()