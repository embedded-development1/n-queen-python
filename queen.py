class Board():
    def __vaildPlacement(self, row, column, size, board):
        for i in range(column, -1, -1):
            if(board[row][i] == 1):
                return False

        for i,j in zip(range(row, -1, -1), range(column, -1, -1)):
            if(board[i][j] == 1):
                return False

        for i,j in zip(range(row, size, 1), range(column, -1, -1)):
            if(board[i][j] == 1):
                return False
        return True

    def __BackTracking(self, row, column, numOfQueens, boardsize, board):
        if(column == numOfQueens):
            return True
        for i in range(boardsize):
            newRow = (row + i) % boardsize
            if(self.__vaildPlacement(newRow, column, boardsize, board) == True):
                board[newRow][column] = 1
                if(self.__BackTracking(newRow, column + 1, numOfQueens, boardsize, board) == True):
                    return True
                board[newRow][column] = 0
        return False

    def __DrawBoard(self, boardsize, board):
        for i in range(boardsize):
            for j in range(boardsize):
                print("---", end="")
            print("\n")
            for j in range(boardsize):
                if(board[i][j] == 1):
                    print("|X|", end="")
                else:
                    print("| |", end="")
            print("\n")
        for j in range(boardsize):
            print("---", end="")
        print("\n")

    def PopulateBoard(self, boardsize, numOfQueens, startRow):
        board = [[0] * boardsize for i in range(boardsize)]

        if(numOfQueens > boardsize):
            print("Too many queens or too small board")
        elif(self.__BackTracking(startRow, 0, numOfQueens, boardsize, board) == False):
            print("Solution does not work")
        else:
            self.__DrawBoard(boardsize, board)
